package ihm;

import java.util.Scanner;

import dao.CouleurDaoImpl;
import dao.EnergieDaoImpl;
import dao.IDao;
import dao.IVoitureDao;
import dao.MarqueDaoImpl;
import dao.ModeleDaoImpl;
import dao.VoitureDaoImpl;
import dto.DtoCouleur;
import dto.DtoEnergie;
import dto.DtoMarque;
import dto.DtoModele;
import dto.DtoVoiture;
import entity.Couleur;
import entity.Energie;
import entity.Marque;
import entity.Modele;
import entity.Voiture;
import service.ServiceCouleur;
import service.ServiceEnergie;
import service.ServiceMarque;
import service.ServiceModele;
import service.ServicePuissance;
import service.ServiceVoiture;

/**
 * La classe Menu contient le menu
 *
 * 
 * @authors ag/kb/lf
 */

public class Menu {

	public static void main(String args []) {

		Scanner sc = new Scanner(System.in);
		IDao<Voiture> voitureDao = new VoitureDaoImpl();
		IVoitureDao  iVoitureDao = new VoitureDaoImpl();
		IDao<Marque> marqueDao = new MarqueDaoImpl ();
		IDao<Modele> modeleDao = new ModeleDaoImpl ();
		IDao<Energie> energieDao = new EnergieDaoImpl ();
		IDao<Couleur> couleurDao = new CouleurDaoImpl();

		String menu = "";
		String sousmenu = "";
		boolean fin = true;
		boolean sousfin=true;


		/** @param
		 *  menu : numéro saisi dans le menu 
		 *  fin : arrêt du programme
		 */ 

		while (fin) {

			System.out.println();
			System.out.println("      **************");
			System.out.println("      *    MENU    *");
			System.out.println("      **************");
			System.out.println();
			System.out.println("0- Arreter le programme");
			System.out.println("1- Menu Voiture");
			System.out.println("2- Menu Marque");
			System.out.println("3- Menu Modele");
			System.out.println("4- Menu Energie");
			System.out.println("5- Menu Couleur");
			System.out.println("6- Menu Puissance");
			System.out.print("-> ");
			menu = Clavier.lireTxt(sc, "");
			System.out.println();

			switch (menu) {
			case "0":
				System.out.println("0 - Arrêt du programme");
				fin = false;
				break;

			case "1":
				System.out.println("1- Menu Voiture");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      **************************");
					System.out.println("      *    SOUS-MENU VOITURE   *");
					System.out.println("      **************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer une voiture");
					System.out.println("2- Lister une voiture");
					System.out.println("3- Mettre à jour une voiture");
					System.out.println("4- Supprimer une voiture");
					System.out.println("5- Lister les voitures par couleur");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Créer une voiture");
						System.out.print("Saisir l'immatriculation : ");
						String im = Clavier.lireTxt(sc, "");
						System.out.print("Saisir le modèle : ");
						String mod = Clavier.lireTxt(sc, "");
						System.out.print("Saisir la couleur : ");
						String coul = Clavier.lireTxt(sc, "");

						ServiceVoiture.creerVoiture(im, mod, coul);

						break;

					case "2":
						System.out.println("2- Lister une voiture");
						System.out.print("Saisir l'immatriculation : ");
						im = Clavier.lireTxt(sc, "");
						ServiceVoiture.listerImmatriculation(im);

						break;

					case "3":
						System.out.println("3- Mettre à jour une voiture");

						break;

					case "4":
						System.out.println("4- Supprimer une voiture");

						break;

					case "5":
						System.out.println("5- Lister les voitures par couleur");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}

				break;

			case "2":
				System.out.println("2- Menu Marque");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU MARQUE   *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer une marque");
					System.out.println("2- Lister une marque");
					System.out.println("3- Mettre à jour une marque");
					System.out.println("4- Supprimer une marque");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Créer une marque");
						System.out.print("Saisir la marque : ");
						String marq = Clavier.lireTxt(sc, "");

						ServiceMarque.creerMarque(marq);
						break;

					case "2":
						System.out.println("2- Lister une marque");
						marq = Clavier.lireTxt(sc, "");
						break;

					case "3":
						System.out.println("3- Mettre à jour une marque");

						break;

					case "4":
						System.out.println("4- Supprimer une marque");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}




				break;

			case "3":
				System.out.println("3- Menu Modele");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU MODELE   *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer un modele");
					System.out.println("2- Lister un modele");
					System.out.println("3- Mettre à jour un modeles");
					System.out.println("4- Supprimer un modele");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Créer un modele");
						System.out.print("Saisir le modele : ");
						String mo= Clavier.lireTxt(sc, "");
						System.out.print("Saisir la puissance : ");
						String pui = Clavier.lireTxt(sc, "");
						System.out.print("Saisir l'energie : ");
						String ener = Clavier.lireTxt(sc, "");
						System.out.print("Saisir la marque : ");
						String mar = Clavier.lireTxt(sc, "");

						ServiceModele.creerModele(pui, ener, mar, mo);

						break;

					case "2":
						System.out.println("2- Liste un modele");

						break;

					case "3":
						System.out.println("3- Mettre à jour un modele");

						break;

					case "4":
						System.out.println("4- Supprimer un modele");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}
				break;

			case "4":
				System.out.println("4- Menu Energie");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU ENERGIE  *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer une energie");
					System.out.println("2- Lister une energie");
					System.out.println("3- Mettre à jour une energie");
					System.out.println("4- Supprimer une energie");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					//					sc.nextLine();
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.println("1- Créer une energie");

						System.out.print("Saisir l'energie : ");
						String ener = Clavier.lireTxt(sc, "");

						ServiceEnergie.creerEnergie(ener);

						break;

					case "2":
						System.out.println("2- Lister une energie");

						break;

					case "3":
						System.out.println("3- Mettre à jour une energie");

						break;

					case "4":
						System.out.println("4- Supprimer une energie");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}
				break;
			case "5":
				System.out.println("5- Menu Couleur");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU COULEUR  *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer une couleur");
					System.out.println("2- Lister une couleur");
					System.out.println("3- Mettre à jour une couleur");
					System.out.println("4- Supprimer une couleur");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.print("Saisir la couleur : ");
						String coul = Clavier.lireTxt(sc, "");

						ServiceCouleur.creerCouleur(coul);

						break;

					case "2":
						System.out.println("2- Lister une couleur");
						System.out.print("Saisir la couleur : ");
						coul = Clavier.lireTxt(sc, "");
						ServiceCouleur.listerCouleur(coul);
						
						
						break;

					case "3":
						System.out.println("3- Mettre à jour une couleur");

						break;

					case "4":
						System.out.println("4- Supprimer une couleur");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}
				break;

			case "6":
				System.out.println("6- Menu Puissance");
				sousfin=true;

				while (sousfin) {

					System.out.println();
					System.out.println("      *************************");
					System.out.println("      *    SOUS-MENU PUISSANCE  *");
					System.out.println("      *************************");
					System.out.println();
					System.out.println("0- Retourner au menu principal");
					System.out.println("1- Créer une puissance");
					System.out.println("2- Lister une puissance");
					System.out.println("3- Mettre à jour une puissance");
					System.out.println("4- Supprimer une puissance");
					System.out.print("-> ");
					sousmenu = Clavier.lireTxt(sc, "");
					System.out.println();

					switch (sousmenu) {
					case "0":
						System.out.println("0 - Retour au menu principal");
						sousfin = false;
						break;

					case "1":
						System.out.print("Saisir la puissance : ");
						String pui = Clavier.lireTxt(sc, "");

						ServicePuissance.creerPuissance(pui);

						break;

					case "2":
						System.out.println("2- Lister une puissance");

						break;

					case "3":
						System.out.println("3- Mettre à jour une puissance");

						break;

					case "4":
						System.out.println("4- Supprimer une puissance");

						break;

					default:
						System.out.println("Mauvaise saisie");
						break;

					}

				}

				break;


			default:
				System.out.println("Mauvaise saisie");
				break;
			}
		}















		sc.close();
	}
}

