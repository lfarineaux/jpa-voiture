package entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_couleur")
@NamedQueries({ 
	@NamedQuery(name = "findAll", query = "SELECT p FROM Couleur p"),
		@NamedQuery(name = "findByCoul", query = "SELECT p FROM Couleur p where p.couleur= :fNameParam"),
		})
public class Couleur {

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_couleur", length = 3, nullable = false)
	private int id_couleur;

	@Column(name = "label_couleur", length = 25, nullable = false)
	private String couleur;

	@OneToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.LAZY, mappedBy = "modele")
	private Set<Voiture> voitures;


	public Couleur (String c) {
		super();
		this.couleur = c;
	}


}
