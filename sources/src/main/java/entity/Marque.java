package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_marque")

@NamedQueries({ 
	@NamedQuery(name = "mFindAll", query = "SELECT p FROM Marque p"),
	@NamedQuery(name = "findByMarq", query = "SELECT p FROM Marque p where p.marque= :fNameParam"), })
public class Marque {
	
	
	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_marque", length = 3, nullable = false)
	private int id_marque;
	
	@Column(name = "label_marque", length = 25, nullable = false)
	private String marque;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "marque")
	private Set<Modele> modeles;
	
//	@OneToMany( fetch = FetchType.LAZY, mappedBy = "idModele")
//	private Set<Modele> modeles;
	
	public Marque (String m) {
		super();
		this.marque = m;
				
	}
	

}
