package entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_voiture")

@NamedQueries({ 
	@NamedQuery(name = "findByImmat", query = "SELECT p FROM Voiture p where p.immatriculation= :fNameParam"), })
public class Voiture {

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_voiture;
	
	@Column(name = "label_immatriculation", length = 25, nullable = false)
	private String immatriculation;

	@OneToOne(optional = true)
	private Modele modele;
	
	@ManyToOne(optional = true)
	private Couleur couleur;
	
	public Voiture (String i, Modele m,Couleur c) {
		super ();
		this.immatriculation=i;
		this.modele=m;
		this.couleur=c;
	}
	
}

