package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_energie")

@NamedQueries({ 
	@NamedQuery(name = "eFindAll", query = "SELECT p FROM Energie p"),
	@NamedQuery(name = "findByEner", query = "SELECT p FROM Energie p where p.energie= :fNameParam"), })
public class Energie {

	
	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_energie", length = 3, nullable = false)
	private int idEnergie;
	
	@Column(name = "label_energie", length = 25, nullable = false)
	private String energie;

	@ManyToMany(
			fetch = FetchType.LAZY,
			mappedBy = "energies")
	Set<Modele> modeles;
	
//	@ManyToMany(
//			fetch = FetchType.LAZY, 
//			mappedBy = "energies")
//	Set<Modele> modeles;
	
	
	public Energie (String e) {
		super();
		this.energie = e;
				
	}
}
