package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_modele")

@NamedQueries({ 
	@NamedQuery(name = "findByModel", query = "SELECT p FROM Modele p where p.modele= :fNameParam"), 
 })
public class Modele {

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_modele", length = 3, nullable = false)
	private int idModele;
	
	@Column(name = "label_modele", length = 25, nullable = false)
	private String modele;
	
	@ManyToOne()
	@JoinColumn(name = "id_puissance")
	private Puissance puissance;
	
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinTable(name = "t_modele_energie", joinColumns = { @JoinColumn(name = "id_modele") },
	inverseJoinColumns = {@JoinColumn(name = "id_energie") })
	private Set<Energie> energies;
	
//	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
//	@JoinTable(name = "t_modele_energie", joinColumns = { @JoinColumn(name = "id_modele") },
//	inverseJoinColumns = {@JoinColumn(name = "id_energie") })
//	private Set<Energie> energies;
	
//	@ManyToOne(optional = true,cascade= {CascadeType.PERSIST})
//	@JoinColumn(name = "label_marque")
//	private Marque marque;
	
	@ManyToOne()
	@JoinColumn(name = "id_marque")
	private Marque marque;
	
	public Modele (String m,Puissance p,Set<Energie> e,Marque marque) {
		super();
		this.modele = m;
		this.energies=e;
		this.puissance = p;
		this.marque=marque;
					
	}
	
}
