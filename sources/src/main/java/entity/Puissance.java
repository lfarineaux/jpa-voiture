package entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "t_puissance")
@NamedQuery(name = "pFindAll", query = "SELECT p FROM Puissance p")
public class Puissance {

	
	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_puissance", length = 3, nullable = false)
	private int id_puissance;
	
	@Column(name = "label_puissance", length = 25, nullable = false)
	private String puissance;
	
//	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "puissance")
//	private Set<Modele> modeles;
	
//	@OneToMany( fetch = FetchType.LAZY, mappedBy = "puissance")
//	private Set<Modele> modeles;
	
	public Puissance (String p) {
		super ();
		this.puissance=p;
	}

	 
	
	
}
