package dao;

import java.util.List;

import entity.Modele;
import entity.Voiture;


public interface IModeleDao extends IDao<Modele>{
	
	public List<Voiture> findVoitureMaxPowerByMarque();
	
	public Modele findByModele(String im) ;
	
}
