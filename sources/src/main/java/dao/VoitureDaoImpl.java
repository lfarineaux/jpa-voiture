package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Voiture;


public class VoitureDaoImpl extends AbstractDao<Voiture>  implements IVoitureDao {

	@Override	
	public List<Voiture> findVoitureByCouleur(String c) {

		return null;
	}


	@Override
	public Voiture findByImmat(String im) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> res = em.createNamedQuery("findByImm", Voiture.class);
			res.setParameter("fNameParam", im);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
	}
	
	
	
	
}
