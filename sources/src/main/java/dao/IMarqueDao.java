package dao;

import java.util.List;

import entity.Marque;
import entity.Modele;
import entity.Voiture;


public interface IMarqueDao extends IDao<Marque>{
	
	public Marque findByMarque(String im) ;
	
}
