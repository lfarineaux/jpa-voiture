package dao;

import java.util.Collection;
import java.util.List;

import entity.Voiture;


public interface IVoitureDao extends IDao<Voiture>{
	
	public List<Voiture> findVoitureByCouleur(String c);
	
	public Voiture findByImmat (String n);
}
