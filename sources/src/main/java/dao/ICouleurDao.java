package dao;

import java.util.List;

import entity.Couleur;
import entity.Modele;
import entity.Voiture;


public interface ICouleurDao extends IDao<Couleur>{
	
	
	public Couleur findByCouleur(String im) ;
	
	public List<Couleur> findAll();
}
