package dao;

import java.util.List;

import entity.Couleur;
import entity.Energie;
import entity.Modele;
import entity.Voiture;


public interface IEnergieDao extends IDao<Energie>{
	
	
	public Energie findByEnergie(String im) ;
	
}
