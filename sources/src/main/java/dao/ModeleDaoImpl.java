package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Modele;
import entity.Voiture;


public class ModeleDaoImpl extends AbstractDao<Modele>  implements IModeleDao {

	@Override	
	public List<Voiture> findVoitureMaxPowerByMarque() {
		
		return null;
	}

	public Modele findByModele(String im) {
	
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Modele> res = em.createNamedQuery("findByModel", Modele.class);
			res.setParameter("fNameParam", im);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
		
	}
	
}
