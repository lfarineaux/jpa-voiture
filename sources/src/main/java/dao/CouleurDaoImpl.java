package dao;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Couleur;


public class CouleurDaoImpl extends AbstractDao<Couleur> implements ICouleurDao {

	public Couleur findByCouleur(String im) {
		
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Couleur> res = em.createNamedQuery("findByCoul", Couleur.class);
			res.setParameter("fNameParam", im);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
		
		
		
	}
	
	@Override
	public List<Couleur> findAll() {
		return this.findAll();
	}
	
	
}
