package dao;

import entity.Voiture;

public interface IDao<T> {
	public T add(T entity);
//	public T find(Object id);
	public T update(T entity);
	public void remove(Object pk);
}
