package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Marque;
import entity.Modele;
import entity.Voiture;


public class MarqueDaoImpl extends AbstractDao<Marque>implements IMarqueDao {

	public Marque findByMarque(String im) {
		
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Marque> res = em.createNamedQuery("findByMarq", Marque.class);
			res.setParameter("fNameParam", im);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
		
	}
}
