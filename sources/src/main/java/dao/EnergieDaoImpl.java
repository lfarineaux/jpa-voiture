package dao;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entity.Energie;


public  class EnergieDaoImpl extends AbstractDao<Energie> implements IEnergieDao {

	public Energie findByEnergie(String im) {
		
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Energie> res = em.createNamedQuery("findByEner", Energie.class);
			res.setParameter("fNameParam", im);
			return res.getSingleResult();
		} finally {
			closeEntityManager(em);
		}
		
	}

}
