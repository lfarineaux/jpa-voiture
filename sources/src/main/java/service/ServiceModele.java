package service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import dao.EnergieDaoImpl;
import dao.IDao;
import dao.MarqueDaoImpl;
import dao.ModeleDaoImpl;
import dao.PuissanceDaoImpl;
import entity.Energie;
import entity.Marque;
import entity.Modele;
import entity.Puissance;

public class ServiceModele {

	static IDao<Modele> modeleDao = new ModeleDaoImpl ();
	
	
	public static Modele creerModele (String pui,String ener,String mar, String mo) {
		
		
		PuissanceDaoImpl pdao = new PuissanceDaoImpl();
		Puissance p = pdao.findAllByNamedQuery("pFindAll").stream().filter(pe->pe.getPuissance().equals(pui)).findAny().get();
		
		EnergieDaoImpl edao = new EnergieDaoImpl();
		Energie e = edao.findAllByNamedQuery("eFindAll").stream().filter(pe->pe.getEnergie().equals(ener)).findAny().get();
		
		List <Energie> energies = Arrays.asList(e);
		HashSet <Energie> set = new HashSet<>(energies); 
		
		MarqueDaoImpl mdao = new MarqueDaoImpl();
		Marque ma = mdao.findAllByNamedQuery("mFindAll").stream().filter(pe->pe.getMarque().equals(mar)).findAny().get();
		
		Modele m=new Modele(mo, p, set, ma);

		modeleDao.add(m);
		
		return m;
	}
	
	
	
}
