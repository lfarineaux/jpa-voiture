package service;

import dao.CouleurDaoImpl;
import dao.EnergieDaoImpl;
import dao.IDao;
import dao.IVoitureDao;
import dao.MarqueDaoImpl;
import dao.ModeleDaoImpl;
import dao.VoitureDaoImpl;
import entity.Couleur;
import entity.Energie;
import entity.Marque;
import entity.Modele;
import entity.Voiture;

public class ServiceEnergie {

	static IDao<Energie> energieDao = new EnergieDaoImpl ();
	
	
	public static Energie creerEnergie (String  ener) {
		Energie e = new Energie(ener);

		energieDao.add(e);
		
		return e;
	}
	
}
