package service;

import dao.IDao;
import dao.IVoitureDao;
import dao.VoitureDaoImpl;
import entity.Couleur;
import entity.Modele;
import entity.Voiture;

public class ServiceVoiture {

	static IDao<Voiture> voitureDao = new VoitureDaoImpl();
	static IVoitureDao  iVoitureDao = new VoitureDaoImpl();

	public static Voiture creerVoiture(String im,String mod, String coul) {

		Modele model=new Modele();
		model.setModele(mod);
		Couleur couleur=new Couleur(coul);
		Voiture v=new Voiture (im,model,couleur);

		voitureDao.add(v);

		return v;
	}

	public static Voiture listerImmatriculation(String im) {
		Voiture v=new Voiture();

		return iVoitureDao.findByImmat(im);
	}

}
