package service;

import dao.CouleurDaoImpl;
import dao.EnergieDaoImpl;
import dao.IDao;
import dao.IVoitureDao;
import dao.MarqueDaoImpl;
import dao.ModeleDaoImpl;
import dao.VoitureDaoImpl;
import entity.Couleur;
import entity.Energie;
import entity.Marque;
import entity.Modele;
import entity.Voiture;

public class ServiceMarque {
	
	static IDao<Marque> marqueDao = new MarqueDaoImpl ();
	
	public static Marque creerMarque (String marq) {
		Marque m = new Marque(marq);
		
		marqueDao.add(m);
		
		return m;

	}
	
}
