package service;

import java.util.Scanner;

import dao.CouleurDaoImpl;
import dao.EnergieDaoImpl;
import dao.IDao;
import dao.IVoitureDao;
import dao.MarqueDaoImpl;
import dao.ModeleDaoImpl;
import dao.PuissanceDaoImpl;
import dao.VoitureDaoImpl;
import entity.Couleur;
import entity.Energie;
import entity.Marque;
import entity.Modele;
import entity.Puissance;
import entity.Voiture;

public class ServicePuissance {
	
	static IDao<Puissance> puissanceDao = new PuissanceDaoImpl();
	
	public static Puissance creerPuissance (String pui) {
		Puissance p = new Puissance (pui);

		puissanceDao.add(p);
		
		return p;
	}
	
}
