
create table t_couleur(
id_couleur serial primary key,
label_couleur varchar(25)  unique
);

insert into t_couleur (label_couleur)values('rouge');

insert into t_couleur (label_couleur)values('vert');

select * from t_couleur as c;

create table t_energie(
id_energie serial ,
label_energie varchar(25) primary key
);

insert into t_energie (label_energie)values('DIESEL');

insert into t_energie (label_energie)values('ESSENCE');

insert into t_energie (label_energie)values('ELECTRIQUE');

select * from t_energie as e;

create table t_marque(
id_marque serial,
label_marque varchar(25) primary key
);

insert into t_marque(label_marque)values ('AUDI');

insert into t_marque(label_marque)values ('MERCEDES');

insert into t_marque(label_marque)values ('BMW');

select * from t_marque;

create table t_puissance(
id_puissance serial,
label_puissance varchar(25) primary key
);

insert into t_puissance (label_puissance) values ('90ch');

insert into t_puissance (label_puissance) values ('105ch');

insert into t_puissance (label_puissance) values ('110ch');

insert into t_puissance (label_puissance) values ('130ch');

select * from t_puissance;

create table t_modele(
id_modele serial primary key,
label_modele varchar(25) ,
label_puissance varchar(25),
label_energie varchar(25),
label_marque varchar(25)
);

alter table t_modele add constraint fk_label_puissance foreign key (label_puissance) references t_puissance(label_puissance);

alter table t_modele add constraint fk_label_energie foreign key (label_energie) references t_energie(label_energie);

alter table t_modele add constraint fk_label_marque foreign key (label_marque) references t_marque(label_marque);

insert into t_modele (label_modele,label_puissance,label_energie,label_marque) values('320','110ch','ELECTRIQUE','BMW');

insert into t_modele (label_modele,label_puissance,label_energie,label_marque) values('319','110ch','ELECTRIQUE','BMW');


select * from t_modele;

create table t_voiture(
id_voiture serial primary key,
label_immatriculation varchar(25),
label_modele varchar(25) ,
label_couleur varchar(25)
);

alter table t_voiture add constraint fk_lab_model foreign key (label_model) references t_modele(id_modele);

alter table t_voiture add constraint fk_lab_couleur foreign key (label_couleur) references t_couleur(label_couleur);

select * from t_voiture as v;

insert into t_voiture (label_immatriculation,label_modele,label_couleur) values ('15AMG59','320','rouge');



